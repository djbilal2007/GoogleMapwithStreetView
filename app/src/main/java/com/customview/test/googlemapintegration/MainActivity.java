package com.customview.test.googlemapintegration;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.customview.test.googlemapintegration.utility.DelayAutoCompleteTextView;
import com.customview.test.googlemapintegration.utility.MyCurrentLocation;
import com.customview.test.googlemapintegration.utility.PlacesAutoCompleteAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, OnStreetViewPanoramaReadyCallback {

    static final LatLng PARIS = new LatLng(48.856272,2.297756);
    static final LatLng LONDON = new LatLng(51.509865, -0.118092);
    static final LatLng TAJ_MAHAL = new LatLng(27.1724278,78.0419971);
    static LatLng dynamicLocation = TAJ_MAHAL;

    private ViewFlipper viewFlipper;
    private ImageButton switch_btn, current_location_btn, get_distance_btn;

    private Geocoder geocoder;
    private StreetViewPanorama streetViewPanorama;

    //Constants
    private Integer THRESHOLD = 1;
    private DelayAutoCompleteTextView geo_autocomplete;
    private GoogleMap globalGoogleMap;

    private MyCurrentLocation currentLocation;

    private List<String> resultList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isInternetConnected(MainActivity.this)){
            Toast.makeText(this, "Please check Internet Connection", Toast.LENGTH_SHORT).show();
        }

        //Current Location Service
        currentLocation = new MyCurrentLocation(this);
        currentLocation.buildGoogleApiClient(this);

        geo_autocomplete = (DelayAutoCompleteTextView)findViewById(R.id.geo_autocomplete);
        geo_autocomplete.setThreshold(THRESHOLD);

        //geo_autocomplete.setAdapter(new GeoAutoCompleteAdapter(this));
        geo_autocomplete.setAdapter(new PlacesAutoCompleteAdapter(MainActivity.this, R.layout.geo_search_result));

        geo_autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String location = (String) parent.getItemAtPosition(position);
                geo_autocomplete.setText(location);

                Address address = getAddressFromName(location);
                double lat = address.getLatitude();
                double lon = address.getLongitude();


/*                GeoSearchResult geoSearchResult = (GeoSearchResult)parent.getItemAtPosition(position);
                geo_autocomplete.setText(geoSearchResult.getAddress());

                double lat = geoSearchResult.getLatitude();
                double lon = geoSearchResult.getLongitude();
*/
                LatLng newLatLng = new LatLng(lat, lon);
                dynamicLocation = newLatLng;
                showLocation(newLatLng);
            }
        });


        geocoder = new Geocoder(this, Locale.getDefault());

        viewFlipper = (ViewFlipper)findViewById(R.id.viewflipper);
        switch_btn = (ImageButton)findViewById(R.id.switch_btn);
        get_distance_btn = (ImageButton)findViewById(R.id.get_distance_btn);
        current_location_btn = (ImageButton)findViewById(R.id.current_location_btn);

        SupportMapFragment supportMapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        StreetViewPanoramaFragment streetViewPanoramaFragment = (StreetViewPanoramaFragment) getFragmentManager().findFragmentById(R.id.street_view_panorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);

        switch_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFlipper.showNext();
            }
        });
        current_location_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {                checkPermissionforGPS();
            }
        });
        get_distance_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GetDistanceActivity.class);
                intent.putExtra("lat", dynamicLocation.latitude);
                intent.putExtra("lon", dynamicLocation.longitude);
                startActivity(intent);
                finish();
            }
        });
    }

    private Address getAddressFromName(String location) {
        //List<Address> addresses = new ArrayList<>();
        Address address = null;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            address = geocoder.getFromLocationName(location, 1).get(0);
            /*for(Address address : addresses){
                Log.d("MainActivity", address.toString());
            }*/
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    private static final int REQUEST_CODE_PERMISSION = 2;

    private void checkPermissionforGPS() {
        try{
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_PERMISSION);
            }else if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_PERMISSION);
            }else {
                getCurrentLocation();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getCurrentLocation() {
        currentLocation.start();
        LatLng latLng = currentLocation.getLocation();
        dynamicLocation = latLng;
        showCurrentLocationOnMap(dynamicLocation);
    }

    private void showCurrentLocationOnMap(LatLng latLng) {
        globalGoogleMap.clear();

        MarkerOptions marker2 = new MarkerOptions()
                .position(latLng)
                .title("Marker")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        globalGoogleMap.addMarker(marker2);
        globalGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(17)
                .build();

        globalGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        globalGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        streetViewPanorama.setPosition(latLng);
    }


    private void showLocation(LatLng newLatLng) {
        globalGoogleMap.clear();

        MarkerOptions marker2 = new MarkerOptions()
                .position(newLatLng)
                .title("Marker")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        globalGoogleMap.addMarker(marker2);
        globalGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(newLatLng));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(newLatLng)
                .zoom(12)
                .build();

        globalGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        globalGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        streetViewPanorama.setPosition(newLatLng);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        globalGoogleMap = googleMap;

        MarkerOptions marker1 = new MarkerOptions()
                .position(TAJ_MAHAL)
                .title("Taj Mahal")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        globalGoogleMap.addMarker(marker1);
        globalGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(TAJ_MAHAL));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(TAJ_MAHAL)
                .zoom(17)
                .tilt(30)
                .build();

        globalGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        globalGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        globalGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                dynamicLocation = latLng;
                String add = getAddressName(latLng);

                Toast.makeText(MainActivity.this, add, Toast.LENGTH_SHORT).show();

                globalGoogleMap.clear();
                MarkerOptions marker2 = new MarkerOptions()
                        .position(latLng)
                        .title("Marker")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

                globalGoogleMap.addMarker(marker2);

                streetViewPanorama.setPosition(latLng);
            }
        });
    }

    private String getAddressName(LatLng latLng) {
        StringBuilder sb = new StringBuilder();
        List<Address> addresses = new ArrayList<Address>();
        try{
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address address = addresses.get(0);

        if(address != null) {
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                sb.append(address.getAddressLine(i) + "\n");
            }
        }
        return sb.toString();
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
        //streetViewPanorama.setPosition(new LatLng(-33.87365, 151.20689));

        this.streetViewPanorama = panorama;
        panorama.setPosition(TAJ_MAHAL);
    }

    public static boolean isInternetConnected(Context mContext) {

        try {
            ConnectivityManager connect = null;
            connect = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connect != null) {
                NetworkInfo resultMobile = connect.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                NetworkInfo resultWifi = connect.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if ((resultMobile != null && resultMobile.isConnectedOrConnecting()) || (resultWifi != null && resultWifi.isConnectedOrConnecting())) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        AlertDialog.Builder build = new AlertDialog.Builder(this);
        build.setMessage("Are you sure want to exit?");
        build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        build.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        build.show();
    }

}



//For last location
       /* gps = new GPSTracker(MainActivity.this);
        if(gps.canGetLocation()){
            double lat = gps.getLatitude();
            double lon = gps.getLongitude();
            LatLng latLng = new LatLng(lat, lon);

            if (latLng != null){
                Log.d("MainActivity", "Lat: " + latLng.latitude + "Lon: " + latLng.longitude);
            }
            showCurrentLocationOnMap(latLng);

        }else {
            gps.showSettingsAlert();
        }*/



