package com.customview.test.googlemapintegration.utility;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.customview.test.googlemapintegration.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Pratik Gaikwad on 14,March,2017
 * Email pratik.gaikwad@ratufa.com.
 */
public class GeoAutoCompleteAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<GeoSearchResult> resultList = new ArrayList<GeoSearchResult>();

    public GeoAutoCompleteAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public GeoSearchResult getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       if(convertView == null){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           convertView = inflater.inflate(R.layout.geo_search_result, parent, false);
       }
        ((TextView)convertView.findViewById(R.id.geo_search_result_text)).setText(getItem(position).getAddress());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if(constraint != null){
                    List<GeoSearchResult> locations = findLocations(context, constraint.toString());
                    filterResults.values = locations;
                    filterResults.count = locations.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0){
                    resultList = (List<GeoSearchResult>) results.values;
                    Log.d("Geo","" + resultList.get(0).getAddress());
                    notifyDataSetChanged();
                }else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    private List<GeoSearchResult> findLocations(Context context, String s) {
        List<GeoSearchResult> geo_search_results = new ArrayList<GeoSearchResult>();
        //Geocoder geocoder = new Geocoder(context, context.getResources().getConfiguration().locale);
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;

        try{
            addresses = geocoder.getFromLocationName(s, 15);

            for(int i=0; i<addresses.size(); i++){
                Address address = addresses.get(i);

                if(address.getMaxAddressLineIndex() != -1){
                    geo_search_results.add(new GeoSearchResult(address));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return geo_search_results;
    }
}
