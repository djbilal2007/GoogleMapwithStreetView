package com.customview.test.googlemapintegration.utility;

import android.content.Context;
import android.util.Log;

import com.customview.test.googlemapintegration.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khalid Khan on 29,April,2017
 * Email khalid.khan@ratufa.com.
 */
public class PlaceAPI {

    Context context;
    public static final String TAG = PlaceAPI.class.getSimpleName();
    private static final String URL_HEADER = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    private static final String TYPE = "types=(cities)";

    private static ArrayList<String> resultList = null;

    public PlaceAPI(Context context) {
        this.context = context;
    }

    public List<String> getList(){
        return resultList;
    }

    public List<String> autoComplete(String input){
        resultList = new ArrayList<>();
        String finalUrl = getFinalUrl(input);
        Log.d(TAG, "Final URL: " + finalUrl);
        return downloadJSONusingURLConn(finalUrl);
    }

    private List<String> downloadJSONusingURLConn(String finalURL) {
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            URL url = new URL(finalURL);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;
            char[] buff = new char[1024];
            while((read = in.read(buff)) != -1){
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.d(TAG, "" + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "" + e.getMessage());
            e.printStackTrace();
        } finally {
            if(conn != null){
                conn.disconnect();
            }
        }

        try{
            JSONObject jsonObject = new JSONObject(jsonResults.toString());
            JSONArray predicJSONArray = jsonObject.getJSONArray("predictions");
            for(int i=0; i<predicJSONArray.length(); i++){
                resultList.add(predicJSONArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG, "" + e.getMessage());
        }
        return resultList;
    }

    private String getFinalUrl(String input) {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(URL_HEADER + TYPE);
            sb.append("&key=" + context.getResources().getString(R.string.google_maps_key));
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());
        }
        return sb.toString();
    }
}
