package com.customview.test.googlemapintegration.utility;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Khalid Khan on 29,April,2017
 * Email khalid.khan@ratufa.com.
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable{

    private List<String> resultList;
    Context context;
    int resource;

    PlaceAPI placeAPI;

    public PlacesAutoCompleteAdapter(Context context, int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
        placeAPI = new PlaceAPI(context);
        resultList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                if(constraint != null){
                    resultList = placeAPI.autoComplete(constraint.toString());
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0){
                    notifyDataSetChanged();
                    Log.d(PlaceAPI.TAG, "notifyDataSetChanged");
                }else {
                    notifyDataSetInvalidated();
                    Log.d(PlaceAPI.TAG, "notifyDataSetInvalidated");
                }
            }
        };
        return filter;
    }

    private List<GeoSearchResult> findLocations(Context context, String s) {
        List<GeoSearchResult> geo_search_results = new ArrayList<GeoSearchResult>();
        //Geocoder geocoder = new Geocoder(context, context.getResources().getConfiguration().locale);
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;

        try{
            addresses = geocoder.getFromLocationName(s, 15);

            for(int i=0; i<addresses.size(); i++){
                Address address = addresses.get(i);

                if(address.getMaxAddressLineIndex() != -1){
                    geo_search_results.add(new GeoSearchResult(address));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return geo_search_results;
    }
}
