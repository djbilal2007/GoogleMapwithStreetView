package com.customview.test.googlemapintegration.utility;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.customview.test.googlemapintegration.GetDistanceActivity;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Khalid Khan on 28,April,2017
 * Email khalid.khan@ratufa.com.
 */
public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

    private final String TAG = "ParserTask";

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... params) {

        JSONObject jsonObject;
        List<List<HashMap<String, String>>> routes = null;

        try {
            jsonObject = new JSONObject(params[0]);
            Log.d(TAG, params[0].toString());
            DataParser dataParser = new DataParser();
            Log.d(TAG, dataParser.toString());

            routes = dataParser.parse(jsonObject);
            Log.d(TAG, "Executing Routes");
            Log.d(TAG, routes.toString());

        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());
        }
        return routes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {
        ArrayList<LatLng> points;
        PolylineOptions lineOptions = null;

        // Traversing through all the routes
        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList<>();
            lineOptions = new PolylineOptions();

            // Fetching i-th route
            List<HashMap<String, String>> path = result.get(i);

            // Fetching all the points in i-th route
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);

                points.add(position);
            }

            // Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            lineOptions.width(20);
            lineOptions.color(Color.rgb(0, 179, 253));

            Log.d(TAG,"onPostExecute lineoptions decoded");

        }

        // Drawing polyline in the Google Map for the i-th route
        if(lineOptions != null) {
            //GetDistanceActivity.globalGoogleMap.addPolyline(lineOptions);
            GetDistanceActivity.globalGoogleMap.addPolyline(lineOptions);
        }
        else {
            Log.d(TAG,"without Polylines drawn");
        }
    }
}
