package com.customview.test.googlemapintegration.utility;

import android.location.Address;
import android.util.Log;

/**
 * Created by Pratik Gaikwad on 14,March,2017
 * Email pratik.gaikwad@ratufa.com.
 */
public class GeoSearchResult {

    private Address address;

    public GeoSearchResult(Address address) {
        this.address = address;
    }

    public String getAddress() {
        String display_address = "";

        for(int i=0; i<address.getMaxAddressLineIndex(); i++){
            display_address += address.getAddressLine(i) + ", ";
        }

        Log.d("Geo","Add: " + display_address);
        if(display_address.length() > 0){
            display_address = display_address.substring(0, display_address.length() - 2);
        }
        return display_address;
    }

    @Override
    public String toString() {
        String display_address = "";

        if(address.getFeatureName() != null){
            display_address += address + ", ";
        }

        for(int i=0; i<address.getMaxAddressLineIndex(); i++){
            display_address += address.getAddressLine(i);
        }
        return display_address;
    }

    public double getLatitude(){
        double lat = address.getLatitude();
        return lat;
    }

    public double getLongitude(){
        double lon = address.getLongitude();
        return lon;
    }
}
