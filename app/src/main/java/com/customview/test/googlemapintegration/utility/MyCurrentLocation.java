package com.customview.test.googlemapintegration.utility;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Khalid Khan on 20,April,2017
 * Email khalid.khan@ratufa.com.
 */
public class MyCurrentLocation implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Context context;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    public MyCurrentLocation(Context context) {
        this.context = context;
    }

    public synchronized void buildGoogleApiClient(Context context) {
        this.context = context;
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);
    }

    public void start() {
        if(googleApiClient != null){
            googleApiClient.connect();
        }
    }

    public void stop() {
        if(googleApiClient != null){
            googleApiClient.disconnect();
        }
    }

    public LatLng getLocation(){
        LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        return latLng;
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            Log.d("MyCurrentLocation", "Lat: " + lastLocation.getLatitude() + " Lon: " + lastLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if(lastLocation != null){
            Log.d("MyCurrentLocation", "Lat: " + lastLocation.getLatitude() + " Lon: " + lastLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Location", "Location services connection failed with code " + connectionResult.getErrorCode() + " and " + connectionResult.getErrorMessage());
    }
}
