package com.customview.test.googlemapintegration;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.customview.test.googlemapintegration.utility.ParserTask;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class GetDistanceActivity extends AppCompatActivity implements OnMapReadyCallback{

    private Button calculate_distance_btn;
    private LinearLayout linearLayout;
    private TextView time_txt, distance_txt;

    public static GoogleMap globalGoogleMap;
    private static LatLng latLng1;
    private static LatLng latLng2;
    private MarkerOptions marker1, marker2;

    private static boolean clicked = false;
    private static String result;
    private Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_distance);

        Intent intent = getIntent();
        double lat = intent.getDoubleExtra("lat",0);
        double lon = intent.getDoubleExtra("lon",0);
        latLng1 = new LatLng(lat, lon);

        SupportMapFragment supportMapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.get_distance_map);
        supportMapFragment.getMapAsync(this);

        marker1 = new MarkerOptions()
                .position(latLng1)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        Log.d("GetDistanceActivity", "Location: " + latLng1.latitude + ", " + latLng1.longitude);

        time_txt = (TextView)findViewById(R.id.time_txt);
        distance_txt = (TextView)findViewById(R.id.distance_txt);
        linearLayout = (LinearLayout)findViewById(R.id.linear_layout);

        calculate_distance_btn = (Button)findViewById(R.id.calculate_distance_btn);
        calculate_distance_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLatLngValues();
            }
        });

        geocoder = new Geocoder(this, Locale.getDefault());
    }

    private void checkLatLngValues() {
        if((latLng1 != null) &&(latLng2 != null)){
            calculateDistance();
        }else {
            Toast.makeText(GetDistanceActivity.this, "Please select two points", Toast.LENGTH_SHORT).show();
        }
    }

    private void calculateDistance() {
        String loc1 = latLng1.latitude + "," + latLng1.longitude;
        String loc2 = latLng2.latitude + "," + latLng2.longitude;

        //String URL = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" + loc1 + "&destinations=" + loc2 + "&mode=driving&language=en-EN&sensor=false";
        String URL = "https://maps.googleapis.com/maps/api/directions/json?origin=" + loc1 + "&destination=" + loc2 + "&key=" + getResources().getString(R.string.google_maps_key);

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("GetDistanceActivity", "" + response);
                result = response.toString();
                try {
                    String orig_add = response.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("start_address");
                    String dest_add = response.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getString("end_address");
                    String distance = response.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getString("text");
                    String duration = response.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONObject("duration").getString("text");

                    Log.d("GetDistanceActivity", "Distance from " + orig_add + " to " + dest_add + " is " + distance + " and it can take upto " + duration);

                    linearLayout.setVisibility(View.VISIBLE);

                    time_txt.setText("Time\n" + duration);
                    distance_txt.setText("Distance\n" + distance);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("GetDistanceActivity", "Error: " + error.getMessage());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(GetDistanceActivity.this);

        requestQueue.add(jsonObjectRequest);
        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener() {
            @Override
            public void onRequestFinished(Request request) {
                if(request == jsonObjectRequest){
                    parseJSONData(result);
                }
            }
        });
    }

    private void parseJSONData(String result) {
        Log.d("GetDistanceActivity", "Result: " + result);
        ParserTask parserTask = new ParserTask();
        parserTask.execute(result);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        globalGoogleMap = googleMap;

        MarkerOptions marker = new MarkerOptions()
                .position(latLng1)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        globalGoogleMap.addMarker(marker);
        globalGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng1));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng1)
                .zoom(15)
                .tilt(30)
                .build();

        globalGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        globalGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        globalGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                if(marker1 != null && marker2 != null){
                    globalGoogleMap.clear();
                    marker1 = null;
                    marker2 = null;
                    latLng1 = null;
                    latLng2 = null;
                }
                if(clicked) {
                    latLng1 = latLng;
                    String add = getAddressName(latLng1);
                    marker1 = new MarkerOptions()
                            .position(latLng1)
                            .title(add)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    clicked = !clicked;
                    globalGoogleMap.addMarker(marker1);
                    Log.d("GetDistanceActivity", "Location 1: " + latLng1.latitude + ", " + latLng1.longitude);
                }else {
                    latLng2 = latLng;
                    String add = getAddressName(latLng2);
                    marker2 = new MarkerOptions()
                            .position(latLng2)
                            .title(add)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    clicked = !clicked;
                    globalGoogleMap.addMarker(marker2);
                    Log.d("GetDistanceActivity", "Location 1: " + latLng2.latitude + ", " + latLng2.longitude);
                }
            }
        });
    }

    private String getAddressName(LatLng latLng) {
        StringBuilder sb = new StringBuilder();
        List<Address> addresses = new ArrayList<Address>();
        try{
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address address = addresses.get(0);

        if(address != null) {
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                sb.append(address.getAddressLine(i) + "\n");
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //globalGoogleMap.clear();
        clicked = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(GetDistanceActivity.this, MainActivity.class));
        finish();
    }
}
